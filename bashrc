#----------------------------------------------------------------
# .bashrc
# ---------------------------------------------------------------

# read alias file
[ -e $HOME/.bash_alias ] && source $HOME/.bash_alias

# set prompt to
# username@host[current/directory]
# >>>>>
# in rainbow colors
export PS1="\[\e[31m\]\u\[\e[33m\]@\[\e[32m\]\h\[\e[m\][\[\e[34m\]\w\[\e[m\]]\[\e[34m\]\[\e[31m\]
>\[\e[m\]\[\e[33m\]>\[\e[m\]\[\e[32m\]>\[\e[m\]\[\e[36m\]>\[\e[m\]\[\e[34m\]>\[\e[m\] "


# powerline
#powerline-daemon -q
#POWERLINE_BASH_CONTINUATION=1
#POWERLINE_BASH_SELECT=1
#. /usr/share/powerline/bindings/bash/powerline.sh


# enable bash completion in interactive shells
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


# CUSTOM SETTINGS

# custom scripts
export PATH=$PATH:$HOME/bin

# pip python module stuff
export PATH=$PATH:$HOME/.local/bin
