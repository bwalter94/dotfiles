set nocp

" colors

set t_Co=256		" make colorschemes actually work...
set background=dark
colorscheme nord
syntax enable

" spaces and tabs

set tabstop=4		" number of visual spaces per tab
set softtabstop=4	" number of spaces when editing
set shiftwidth=4    " number of spaces when indenting in visual mode
set expandtab		" tabs are spaces
set linebreak		" allow for soft line breaks
set breakindent     " visually indent long lines 
"set ai				" auto indent
"set si				" smart indent

" UI config

"set number			" show line numbers
set showcmd			" show command in bottom bar
"set cursorline		" highlight current line
set wildmenu		" visual autocomplete for command menu
set showmatch		" highlight matching brackets


" searching

set incsearch		" search as characters are entered
set hlsearch		" highlight matches


" movement

nnoremap j gj		" move vertically by visual line
nnoremap k gk

nnoremap B ^		" move to beginning/end of line
nnoremap E $

nnoremap $ <nop>	" $/^ doesn't do anything
nnoremap ^ <nop>

nnoremap gV `[v`]	" highlight last inserted text

" repone file at the same spot
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" GUI options

if has("gui_running")
	set guioptions-=T	" remove toolbar
	set mouse=a			" enable mouse
endif


" Plugins

call plug#begin('~/.vim/plugged')

    " LaTeX compiling
    Plug 'lervag/vimtex'
    set conceallevel=1          " replace latex code with math
    let g:tex_conceal='abdm'
    let g:vimtex_quickfix_latexlog = { 
          \ 'overfull' : 0,
          \ 'underfull' : 0,
          \ 'packages' : {
          \   'default' : 0,
          \ },
          \}

    " nord colorscheme
    Plug 'arcticicestudio/nord-vim'
    let g:nord_italic = 1
    let g:nord_underline = 1
    let g:nord_italic_comments = 1
    let g:nord_comment_brightness = 12

call plug#end()
