#!/bin/bash

# Make a symbolic link to each dotfile in the home directory

for FILE in *; do
    if [ "$FILE" != "$(basename $0)" ] && [ "$FILE" != "README.md" ]; then
        echo "linking $PWD/$FILE to $HOME/.$FILE"
        ln -s $PWD/$FILE $HOME/.$FILE
    fi
done
